/*
  ==============================================================================

    This file was auto-generated!

  ==============================================================================
*/

#include "MainComponent.h"


//==============================================================================
MainComponent::MainComponent()
{
    setSize (500, 400);
    audioDeviceManager.initialiseWithDefaultDevices(2, 2); //2 inputs, 2 outputs
    audioDeviceManager.addAudioCallback(this);
    
    amplitudeSlider.setSliderStyle(Slider::LinearBarVertical);
    amplitudeSlider.setRange(0.0, 1.0);
    amplitudeSlider.setValue(0.5);
    addAndMakeVisible(amplitudeSlider);
    amplitudeSlider.addListener(this);
}

MainComponent::~MainComponent()
{
    audioDeviceManager.removeAudioCallback(this);
}

void MainComponent::resized()
{
    amplitudeSlider.setBounds(10, 10, getWidth()-20, getHeight()-20);
}



void MainComponent::audioDeviceAboutToStart (AudioIODevice* device)
{
    DBG("audioDeviceAboutToStart called");
}

void MainComponent::audioDeviceStopped()
{
    DBG("audioDeviceStopped called");
}

void MainComponent::audioDeviceIOCallback (const   float** inputChannelData,
                            int numInputChannels,
                            float** outputChannelData,
                            int numOutputChannels,
                            int numSamples)
{
    //DBG("audioDeviceIOCallback called");
    const float *inL = inputChannelData[0];
    const float *inR = inputChannelData[1];
    float *outL = outputChannelData[0];
    float *outR = outputChannelData[1];
    

    
    while(numSamples--)
    {
        
        *outL = *inL;
        *outR = *inR;
        
        *outL *= amplitudeSlider.getValue();
        *outR *= amplitudeSlider.getValue();
        
        inL++;
        inR++;
        outL++;
        outR++;
    }
}





void MainComponent::sliderValueChanged (Slider* slider)
{
    if (slider == &amplitudeSlider)
    {
        float value = amplitudeSlider.getValue();
        std::cout << "amplitudeSlider: " << value << std::endl;

    }
    
    
}